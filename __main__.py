from sklearn.datasets import fetch_openml
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
import numpy as np

mnist = fetch_openml('mnist_784', version=1, cache=True)

X = mnist["data"]
y = mnist["target"].astype(np.uint8)

# Took in 5 times less amount because in case of 60000 waiting was too long
X_train = X[:12000]
y_train = y[:12000]
X_test = X[12000:]
y_test = y[12000:]

svm_clf = SVC(kernel="linear", C=1e10)
svm_clf.fit(X_train, y_train)

# Prediction
y_pred_svm = svm_clf.predict(X_test)
accuracy_svm = accuracy_score(y_test, y_pred_svm)
print(f"SVM Accuracy: {accuracy_svm}")

log_reg = LogisticRegression(max_iter=1000)
log_reg.fit(X_train, y_train)

# Prediction
y_pred_log_reg = log_reg.predict(X_test)
accuracy_log_reg = accuracy_score(y_test, y_pred_log_reg)
print(f"Logistic Regression Accuracy: {accuracy_log_reg}")

# Normalizing
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

svm_clf_scaled = SVC(kernel="linear", C=1e10)
svm_clf_scaled.fit(X_train_scaled, y_train)
y_pred_svm_scaled = svm_clf_scaled.predict(X_test_scaled)
accuracy_svm_scaled = accuracy_score(y_test, y_pred_svm_scaled)
print(f"SVM Accuracy (with normalization): {accuracy_svm_scaled}")

# Gaussian
svm_rbf_clf_scaled = SVC(kernel="rbf", C=1e10)
svm_rbf_clf_scaled.fit(X_train_scaled, y_train)
y_pred_svm_rbf_scaled = svm_rbf_clf_scaled.predict(X_test_scaled)
accuracy_svm_rbf_scaled = accuracy_score(y_test, y_pred_svm_rbf_scaled)
print(f"SVM Accuracy with RBF kernel (with normalization): {accuracy_svm_rbf_scaled}")
